
    #include <stdint.h>
    #include <stdio.h>
    #include "types.h"
    #include "initializing.h" // must be in system init
//-------------------------------------------------------------------------------

    GYRO_data_t GdataX,GdataY,GdataZ;
                //DUS 0,DUS 1,DUS 2
    int main(){


        init_GYRO(&GdataX, 0);
        init_GYRO(&GdataY, 1);
        init_GYRO(&GdataZ, 2);

        read_GYRO(&GdataX);
        //read_GYRO(&GdataY);
        //read_GYRO(&GdataZ);



        printf("%d\n", GdataX.error_code);
        return 0;
    }
