
    #include <stdint.h>

    #include "types.h"

    // ---------------------------------------------------------------------


    // ---------------------------------------------------------------------

    #pragma pack(push, 1)

    typedef struct GYRO_data// __attribute__((packed))
    {
        float32_t  ang_velocity;
        float32_t temperature;
        float32_t scale_factor;
        uint16_t frame_count;
        uint8_t numofGYRO;
        uint8_t length_of_msg;
        uint8_t GYRO_mode; //(0 -> only angl 1-> temp add, 2 -> frame count)
        uint8_t error_code;

    }   GYRO_data_t;

    #pragma pack(pop)


    // ---------------------------------------------------------------------
