
    #include <stdint.h>
    #include "CRC.h"
    #include "types.h"
    #include "initializing.h"

    // ---------------------------------------------------------------------

    #define DUS_STATE 0

    // ---------------------------------------------------------------------

    #pragma pack(push, 1)

    typedef struct   DUS_read_data
    {
        uint16_t header;
        uint32_t ang_velocity;
        uint16_t crc;
    }   DUS_read_data_t;


    typedef struct  DUS_read_data_expand
    {
        uint16_t header;
        uint32_t ang_velocity;
        uint16_t addition;
        uint16_t crc;

    }   DUS_read_data_expand_t;

    typedef struct DUS_read_data_full
    {
        uint16_t header;
        uint32_t ang_velocity;
        uint16_t temperature;
        uint16_t frame_count;
        uint16_t crc;
    }   DUS_read_data_full_t;

    typedef union  DUS_msg
    {
        uint8_t data[12];
        DUS_read_data_t             DUS_read_data;
        DUS_read_data_expand_t      DUS_read_data_expand;
        DUS_read_data_full_t        DUS_read_data_full;
    }   DUS_msg_t;

 #pragma pack(pop)



    // ---------------------------------------------------------------------
