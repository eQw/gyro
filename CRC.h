#include <stdint.h>
//#define mask 0x1021


unsigned int CRC16(uint8_t message[], uint8_t start_msg, uint8_t end_msg)
{
    uint16_t CRC = 0xFFFF;
    uint16_t byte = 0;

    for (uint8_t jj = start_msg; jj<end_msg; jj++){
        byte = message[jj];
        byte <<= 8;
        for (uint8_t ii = 0; ii < 8; ii++) {
           if ((CRC ^ byte) & 0x8000){
                CRC = (CRC << 1) ^ 0x1021;
            }else{
                CRC <<= 1;
                }
           byte  <<= 1;
        }
    }
    return CRC;
}

unsigned int CRC32(uint8_t message[], uint8_t start_msg, uint8_t end_msg) {
    int  j;
    uint32_t byte, crc;

    //ii = 0;
    crc = 0xFFFFFFFF;
    for (uint16_t ii = start_msg; ii <end_msg; ii++)
    {
        //while (message[ii] != 0) {
        byte = message[ii];            // Get next byte.
       //byte = reverse(byte);          // 32-bit reversal.
        for (uint16_t jj = 0; j < 8; j++) {    // Do eight times.
            if ((int)(crc ^ byte) < 0){
               crc = (crc << 1) ^ 0x04C11DB7;
            }
            else{
                    crc = crc << 1;
            }
            byte = byte << 1;          // Ready next msg bit.
        }
        //ii = ii + 1;
    }
    return crc;
}
