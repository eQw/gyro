    #include "OIUS1000.h"
    #include <stddef.h>
    #include <io.h>
   // #include "initializing.h"
//-------------------------------------------------------------------------------

    #define DUS_SCALE_FACTOR_1 1.1967e-07
    #define DUS_SCALE_FACTOR_2 1.1967e-07
    #define DUS_SCALE_FACTOR_3 1.1967e-07

    #define DUS1_STATE 0
    #define DUS2_STATE 0
    #define DUS3_STATE 0

//-------------------------------------------------------------------------------
    float __GYRO(GYRO_data_t *Gdata);

    void init_GYRO(GYRO_data_t *Gdata, uint8_t numOfGyro){

        Gdata -> error_code = 0b00000000;

        Gdata -> numofGYRO = numOfGyro;
        switch(numOfGyro){
            case 0:
                Gdata -> scale_factor = DUS_SCALE_FACTOR_1;
                Gdata -> GYRO_mode = DUS1_STATE;
                break;
            case 1:
                Gdata -> scale_factor = DUS_SCALE_FACTOR_2;
                Gdata -> GYRO_mode = DUS2_STATE;
                break;
            case 2:
                Gdata -> scale_factor = DUS_SCALE_FACTOR_3;
                Gdata -> GYRO_mode = DUS3_STATE;
                break;
            default:
                break;
        }

    }


    int read_GYRO(GYRO_data_t *Gdata)
    {
        DUS_msg_t msg;
        //uint8_t data = 0;

        uint8_t data[12] = {192,192, 100,2  , 0, 0, 131,121,0,0,0,123}; //for test
        uint8_t ii = 0;

        while (ii < sizeof(data)){// data != NULL
            msg.data[ii] = data[ii];
            ii++;
        }

        Gdata -> length_of_msg = ii;
        __GYRO(&Gdata);
    }



    float __GYRO(GYRO_data_t *Gdata)
    {
        DUS_msg_t msg;
        float32_t angular_vel,temperature;
        uint16_t CRC = 0;

        CRC = CRC16(msg.data, sizeof(msg.DUS_read_data.header), 10);//(msg.length - 2)); //CRC16(msg,1st byte, last byte)
        if (msg.DUS_read_data.crc == CRC){
            Gdata -> ang_velocity = msg.DUS_read_data.ang_velocity * Gdata -> scale_factor;

            if( 10 == Gdata -> length_of_msg){

                switch (Gdata -> GYRO_mode){
                        case 1:
                            Gdata -> temperature = msg.DUS_read_data_expand.addition*0.01;
                            break;
                        case 2:
                            Gdata -> frame_count =  msg.DUS_read_data_expand.addition;
                            break;
                        default:
                            break;
                }
                if( 12 == Gdata -> length_of_msg){
                            Gdata -> temperature = msg.DUS_read_data_full.temperature*0.01;
                            Gdata -> frame_count = msg.DUS_read_data_full.frame_count;
                }
            }
        }
        return 0;
    }
