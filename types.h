
#ifndef __TYPES_H__
#define __TYPES_H__

// ---------------------------------------------------------------------

#include <stdint.h>
#include <math.h>

// ---------------------------------------------------------------------

#define RAD(deg)            ((float) (((float) (deg)) 					\
								* (3.14159265f / 180.f)))				\

#define DEG(rad)            ((float) (((float) (rad)) 					\
								* (180.f / 3.14159265f)))				\

// ---------------------------------------------------------------------

typedef long double	float64_t;
typedef float      	float32_t;

typedef union float80
{ 
	uint8_t data[10];

	struct __attribute__((packed)) 
	{
		uint32_t 	m_low;
		uint32_t 	m_high;
		int16_t 	exp;
	};
	
}   float80_t;

// ---------------------------------------------------------------------

float64_t float_80_64(float80_t float80);

// ---------------------------------------------------------------------

#endif /* __TYPES_H__ */


